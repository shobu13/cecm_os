import asyncio
import curses
import os
import sys

from cmd import Cmd
import life


class Commander(Cmd):
    prompt = "guest@CECM:~$ "
    intro = """Linux vps668829 4.9.0-8-amd64 #1 SMP Debian 4.9.144-3.1 (2019-02-19) x86_64 

The programs included with the Debian GNU/Linux system are free software; 
the exact distribution terms for each program are described in the 
individual files in /usr/share/doc/*/copyright. 

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent 
permitted by applicable law. 
Last login: Wed Dec 11 01:31:12 2019 from 91.169.100.118
"""

    def do_life(self, inp):
        """launch game of life"""
        curses.wrapper(life.main)

    def do_clear(self, inp):
        """clear the screen"""
        os.system('cls' if os.name == 'nt' else 'clear')

    def do_exit(self, inp):
        """exit program"""
        os.system('cls' if os.name == 'nt' else 'clear')
        return True

    def preloop(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    do_EOF = do_exit


cmd = Commander()
cmd.cmdloop()
